set shiftwidth=2
set tabstop=2
set expandtab
filetype plugin on
execute pathogen#infect()
syntax on
filetype plugin indent on
set number
set linebreak

set spell
set hlsearch
set smartindent
let g:neocomplcache_enable_at_startup = 1
let g:molokai_original = 1

set completeopt-=preview "Disable the preview window of neocompletecache
set nospell

set foldmethod=syntax   "fold based on indent
set foldnestmax=10      "deepest fold is 10 levels
set nofoldenable        "dont fold by default
set foldlevel=1         "this is just what i use

"Special indent just for html file
"autocmd FileType html setlocal shiftwidth=2 tabstop=2
"autocmd FileType html :setlocal sw=2 ts=2 sts=2

"Add new line below without go into insert mode
nnoremap <silent><S-Enter> O<Esc>
"Add new line about without go into insert mode
nnoremap <silent><C-Enter> o<Esc>

nnoremap <Leader>c :MBEClose<cr>
nnoremap <Leader>t :MBEToggle<cr>
"nnoremap <C-P> :bp<cr>
"nnoremap <C-N> :bn<cr>
nnoremap <Leader><Leader>Q :CommandT<cr>
nnoremap <silent> <F8> :TlistToggle<CR>
"nnoremap <C-S-c> "+y
vnoremap <C-S-C> "+y
vnoremap <C-S-D> "+d
nnoremap <C-S-V> "+gp
vnoremap <C-S-V> "+gp
inoremap <C-S-V> <Esc>"+gp<Esc>
let Tlist_Use_Right_Window = 1
" If you like control + vim direction key to navigate
    " windows then perform the remapping
    "
    noremap <C-J>     <C-W>j
    noremap <C-K>     <C-W>k
    noremap <C-H>     <C-W>h
    noremap <C-L>     <C-W>l

    " If you like control + arrow key to navigate windows
    " then perform the remapping
    "
    "noremap <C-Down>  <C-W>j
    "noremap <C-Up>    <C-W>k
    "noremap <C-Left>  <C-W>h
    "noremap <C-Right> <C-W>l

    " If you like <C-TAB> and <C-S-TAB> to switch buffers
    " in the current window then perform the remapping
    "
    noremap <C-TAB>   :MBEbn<CR>
    noremap <C-S-TAB> :MBEbp<CR>
    noremap <TAB>   :MBEbb<CR>
    " Or, in MRU fashion
    "
    "noremap <C-TAB>   :MBEbf<CR>
    "noremap <C-S-TAB> :MBEbb<CR>
    "
    command! Bf MBEbf
    command! Bb MBEbb
    " If you like <C-TAB> and <C-S-TAB> to switch windows
    " then perform the remapping
    "
    "noremap <C-TAB>   <C-W>w
    "noremap <C-S-TAB> <C-W>W

if has('gui_running')
    "set linespace=5
    "set gfn=Consolas\ 15
    "
    "Start NERDTree
    autocmd VimEnter * NERDTree
    autocmd VimEnter * vertical resize 20
    autocmd VimEnter * wincmd p

    set linespace=3
    set gfn=Monaco\ for\ Powerline\ 13

    set lines=35 columns=110
    colorscheme molokai
endif

set autoindent
set autochdir


let NERDTreeIgnore = ['\.pyc$']
let NERDTreeIgnore += ['\.o$']
let NERDTreeIgnore += ['\.oup$']
let NERDTreeIgnore += ['\.swp$']
let NERDTreeIgnore += ['\.class$']
let NERDTreeIgnore += ['\.d$']
" MiniBufExpl Colors
hi MBENormal               guifg=#808080 guibg=bg
hi MBEChanged              guifg=#CD5907 guibg=bg
hi MBEVisibleNormal        guifg=#5DC2D6 guibg=bg
hi MBEVisibleChanged       guifg=#F1266F guibg=bg
hi MBEVisibleActiveNormal  guifg=#A6DB29 guibg=bg
hi MBEVisibleActiveChanged guifg=#F1266F guibg=bg

" vim-airline
let g:airline_powerline_fonts = 1
let python_highlight_all = 1
