setlocal foldmethod=indent
setlocal foldnestmax=10      "deepest fold is 10 levels
setlocal nofoldenable        "dont fold by default
setlocal foldlevel=1         "this is just what i use
setlocal shiftwidth=2
setlocal tabstop=2
